@extends('layouts.app')

@section('content')
    <div class="col-12">
      @can('isAdmin')
        <div class="jumbotron">
          <h1 class="text-primary">Shelter Dashboard</h1>
          <a class="btn btn-primary" href="/assets/create">Create Assets</a>
          <a class="btn btn-primary" href="/assets">Edit Assets</a>
          <a class="btn btn-primary" href="/transactions">Edit Transactions</a>
        </div>
      @else
        <div class="jumbotron">
          <h1 class="text-primary">Available Shelters</h1>
        </div>
      @endcan
      {{-- error handling show all errors--}}
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @can('isAdmin')
        {{-- admin dashboard --}}
        <table class="table table-dark">
          <thead>
            <tr>
              <th>Name:</th>
              <th>Code name:</th>
              <th>Status:</th>
              <th>Actions:</th>
            </tr>
          </thead>

          <tbody>
            @foreach($categories as $category)
					    <tr>
					      <td><a href="/categories/{{$category->id}}">{{$category->name}}</a></td>
					      <td>{{$category->codename}}</td>
					      <td>
                  @if($category->isActive == 1)
                    <h5 class="text-success">{{'Active'}}</h5>
                  @else
                    <h5 class="text-success">{{'Inactive'}}</h5>
                  @endif
                </td>
                {{-- action buttons --}}
                <td>
                  <a class="btn btn-warning mb-1" href="/categories/{{$category->id}}/edit">Edit</a>
                  <form method="POST" action="/categories/{{$category->id}}">
                    @csrf
                    @method('DELETE')
                    @if($category->isActive == 1)
                      <button type="submit" class="btn btn-danger">Deactivate</button>
                    @else
                      <button type="submit" class="btn btn-success">Reactivate</button>
                    @endif
                  </form>
                </td>
					    </tr>
						@endforeach
          </tbody>
        </table>
      @else
        {{-- non admin view --}}
        <div class="row">
          {{-- catalogue view for non admin users --}}
          @foreach($categories as $category)
            {{-- only display the active products in the catalogue --}}
            @if($category->isActive == 1)
              <div class="card col-6 p-2">
                <div style="width:50%;" class="mx-auto">
                  <img src="{{asset($category->img_path)}}" class="card-img-top" width="100%">
                </div>
                <div class="card-body">
                  <h4 class="card-title"><a href="/categories/{{$category->id}}">{{$category->name}}</a></h4>
                  <p class="card-text">{{$category->description}}</p>
                  {{-- insert table of availability here --}}
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Available rooms:</th>
                        <th>
                          {{$category->available_assets->count()}} / {{$category->assets->count()}}
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <a class="btn btn-success" href="/categories/{{$category->id}}">Request a Room</a>
                </div>
              </div>
            @endif
          @endforeach
        </div>
      @endcan
    </div>
@endsection
