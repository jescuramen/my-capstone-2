@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8 offset-2">
      <div class="jumbotron">
        <h1 class="text-primary">Edit: {{$category->name}}</h1>
      </div>
      <div class="card">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="card-header">
          Edit Shelter
        </div>

        <div class="card-body">
          <form class="" action="/categories/{{$category->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="name">New Shelter name: </label>
              <input class="form-control" type="text" name="name" id="name" value="{{$category->name}}">
            </div>
            <div class="form-group">
              <label for="name">New Codename: </label>
              <input class="form-control" type="text" name="codename" id="codename" value="{{$category->codename}}">
            </div>
            <div class="form-group">
              <label for="description">New Description: </label>
              <input class="form-control" type="text" name="description" id="description" value="{{$category->description}}">
            </div>
            <div class="form-group">
              <label for="image">Image:</label>
              <input type="file" name="image" id="image">
            </div>
            <button type="submit" class="btn btn-success">Edit Shelter</button>
            <a class="btn btn-info mb-1" href="/categories/">Dashboard</a>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
