@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="jumbotron">
        <h1 class="text-primary">Create Shelter</h1>
        <a href="/categories" class="btn btn-info">Dashboard</a>
      </div>
      <div class="card">
        <div class="card-header bg-primary text-white">
          @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div>
              <h4>Add a Shelter</h4>
            </div>
        </div>
        <div class="card-body">
          <form class="" action="/categories" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Shelter name: </label>
              <input class="form-control" type="text" name="name" id="name">
            </div>
            <div class="form-group">
              <label for="name">Codename: </label>
              <input class="form-control" type="text" name="codename" id="codename">
            </div>
            <div class="form-group">
              <label for="description">Description: </label>
              <input class="form-control" type="text" name="description" id="description">
            </div>
            <div class="form-group">
              <label for="image">Image:</label>
              <input type="file" name="image" id="image">
            </div>
            <button type="submit" class="btn btn-success">Add Shelter</button>
            </form>
        </div>
        <div class="card-footer text-center">
          <a class="btn btn-primary" href="/assets/create">Add Assets instead</a>
        </div>
      </div>
    </div>
  </div>
@endsection
