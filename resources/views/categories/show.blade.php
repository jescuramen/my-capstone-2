@extends('layouts.app')

@section('content')
  <div class="col-8 offset-2">
    <div class="card">
      <img src="{{asset($category->img_path)}}" class="card-img-top img-fluid">
      <div class="card-body">
        <h4 class="card-title">{{$category->name}}</h4>
        @can('isAdmin')
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Status</th>
                <th scope="col">Available</th>
                <th scope="col">Not Available</th>
                <th scope="col">Total</th>
              </tr>
            </thead>
            @if($category->isActive === 1)
              <tbody>
                <tr>
                  <td>
                    @if($category->isActive == 1)
                      Active
                    @else
                      Inactive
                    @endif
                  </td>
                  <td>
                    {{-- count the number of assets on this particular shelter --}}
                    @if(count($assets) > 0)
                      {{$isAvailable}}
                    @else
                      0
                    @endif
                  </td>
                  <td>
                    @if(count($assets) > 0)
                      {{$notAvailable}}
                    @else
                      0
                    @endif
                  </td>
                  <td>{{$isAvailable+$notAvailable}}</td>
                </tr>
              </tbody>
            @else
              <tbody>
                <tr>
                  <td>Inactive</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                </tr>
              </tbody>
            @endif
          </table>
        @endcan
        <p class="card-text">{{$category->description}}</p>
        @cannot ('isAdmin')
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Available rooms:</th>
                <th>
                  {{$category->available_assets->count()}} / {{$category->assets->count()}}
                </th>
              </tr>
            </thead>
          </table>
          <form action="/transactions/" method="POST">
            @csrf
            <div class="form-group">
              <input name="id" value="{{$category->id}}" hidden>
              <label for="checkIn">Ingress: </label>
              <input class="text-muted inputDate" type="date" name="ingress" id="ingress">
              <span class="text-danger" id="ingressNotif"></span>
            </div>
            <div class="form-group">
              <label for="checkOut">Egress: </label>
              <input class="text-muted inputDate" type="date" name="egress" id="egress" disabled>
              <span class="text-danger" id="egressNotif"></span>
            </div>
            <button type="submit" class="btn btn-success mb-1" data-id="{{$category->id}}" id="requestBtn" disabled>
              Request
            </button>
          </form>
        @endcannot
        {{-- button leading back to admin dashboard / catalogue --}}
        <a class="btn btn-info mb-1" href="/categories/">Dashboard</a>
        @can('isAdmin')
          <a class="btn btn-warning mb-1" href="/categories/{{$category->id}}/edit">Edit</a>
          <form method="POST" action="/categories/{{$category->id}}">
            @csrf
            @method('DELETE')
            @if($category->isActive == 1)
              <button type="submit" class="btn btn-danger">Deactivate</button>
            @else
              <button type="submit" class="btn btn-success">Reactivate</button>
            @endif
          </form>
        @endcan
      </div>
    </div>
  </div>

  {{-- javascript --}}
  <script src="{{asset('js/requestAsset.js')}}">

  </script>
@endsection
