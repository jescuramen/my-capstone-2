@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="jumbotron">
        <h1 class="text-primary">Create Asset</h1>
        <a href="/categories" class="btn btn-info">Dashboard</a>
      </div>
      <div class="card">
        <div class="card-header bg-primary text-white">
          @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div>
              <h4>Add an Asset</h4>
            </div>
        </div>
        <div class="card-body">
          <form class="" action="/assets" method="POST">
            @csrf
            <div class="form-group">
              <label for="category_id">List of Shelters: </label>
              <select class="form-control" id="category_id" name="shelter">
                <option disabled selected>Select a shelter</option>
                @if(count($categories) > 0)
                  @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <label for="quantity">Quantity: </label>
              <input class="form-control" type="number" name="quantity" id="quantity">
            </div>
            {{-- <div class="form-group">
              <label for="serialNo">Serial Number: </label>
              <input class="form-control" type="text" name="serialNo" id="serialNo">
            </div> --}}
            <button type="submit" class="btn btn-success">Add Asset</button>
            <a href="/categories" class="btn btn-warning">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
