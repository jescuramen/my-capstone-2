@extends('layouts.app')

@section('content')
  <div class="row">
      {{-- error handling show all errors--}}
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @can('isAdmin')
        <div class="col-12">
            <div class="jumbotron">
              <h1 class="text-primary">Assets</h1>
              <a href="/categories" class="btn btn-info">Dashboard</a>
              <a class="btn btn-primary" href="/assets/create">Create Assets</a>
            </div>
          {{-- admin dashboard for assets --}}
          <div class="card-body">
            <form method="GET" action="/assets">
              @csrf
              <div class="form-group">
                <label for="category_id">List of Shelters: </label>
                <select class="form-control" id="category_id" name="shelter">
                  <option disabled selected>Select a shelter</option>
                  @if(count($categories) > 0)
                    @foreach($categories as $category)
                      <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <button type="submit" class="btn btn-success" id="getCatBtn">Filter</button>
            </form>
          </div>
          @foreach($categories as $category)
            <h3>{{$category->name}}</h3>
            <table class="table table-dark">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Code name:</th>
                  <th>Serial Number:</th>
                  <th>Status: </th>
                  <th>Maintenance: </th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $counter = 0;
                @endphp
                @foreach($assets as $asset)
                  @if($category->id == $asset->category_id)
                    @php
                      $counter++;
                    @endphp
                    <tr>
                      <td>{{$counter}}</td>
      					      <td>{{$asset->category->codename}}</td>
                      <td>{{$asset->serialNo}}</td>
                      <td>
                        @if($asset->isAvailable === 1)
                          <h5 class="text-success">{{'Vacant'}}</h5>
                        @else
                          <h5 class="text-danger">{{'Not Available'}}</h5>
                        @endif
                      </td>
      					      <td>
                        @if($asset->isMaintained === 1)
                          <h5 class="text-success">{{'Good'}}</h5>
                        @else
                          <h5 class="text-danger">{{'No Good'}}</h5>
                        @endif
                      </td>
                      {{-- action buttons --}}
                      <td>
                        <form class="mb-1" method="POST" action="/assets/{{$asset->id}}">
                          @csrf
                          @method('DELETE')
                          @if($asset->isAvailable == 1)
                            <button type="submit" class="btn btn-danger">Deactivate</button>
                          @else
                            <button type="submit" class="btn btn-success">Reactivate</button>
                          @endif
                        </form>
                        <form method="POST" action="/assets/{{$asset->id}}">
                          @csrf
                          @method('PUT')
                          @if($asset->isMaintained == 1)
                            <button type="submit" class="btn btn-danger">Down</button>
                          @else
                            <button type="submit" class="btn btn-success">Up</button>
                          @endif
                        </form>
                      </td>
      					    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          @endforeach
      @endcan
    </div>
  </div>
@endsection
