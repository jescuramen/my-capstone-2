@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4>Welcome {{$user->name}}! What would you like to do today?</h4>
                </div>
                @can('isAdmin')
                  <h5 class="card-body">Create</h5>
                  <ul>
                    <li><a href="/categories/create">Create a Shelter</a></li>
                    <li><a href="/assets/create">Create Asset</a></li>
                  </ul>
                  <h5 class="card-body">Manage</h5>
                  <ul>
                    <li><a href="/categories">Manage Shelters</a></li>
                    <li><a href="/assets">Manage Assets</a></li>
                    <li><a href="/transactions">Manage Transactions</a></li>
                  </ul>
                @else
                  <ul>
                    <li><a href="/categories">Request a Shelter</a></li>
                    <li><a href="/transactions">My Transactions</a></li>
                  </ul>
                @endcan
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
