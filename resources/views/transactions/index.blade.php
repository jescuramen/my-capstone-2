@extends('layouts.app')

@section('content')
  @if($user)
    <div class="row">
        {{-- error handling show all errors--}}
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @can('isAdmin')
          {{-- admin view --}}
          <div class="col-12">
            <div class="jumbotron">
              <h1 class="text-primary">Transactions</h1>
              <a href="/categories" class="btn btn-info">Dashboard</a>
              <a class="btn btn-primary" href="/assets/create">Create Assets</a>
            </div>
            @if(session('approve'))
              <div class="alert alert-success" role="alert">
                {{session('approve')}}
              </div>
            @endif
            @if(session('reject'))
              <div class="alert alert-danger" role="alert">
                {{session('reject')}}
              </div>
            @endif
            <h3>Transactions Pending Approval</h3>
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>User</th>
                  <th>Shelter</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($transactions as $transaction)
                  @if($transaction->status_id == 1)
                    <tr>
                      <td><a href="#" data-toggle="modal" data-target="#exampleModalCenter">{{$transaction->user->name}}</a></td>
                      <!-- Modal -->
                      <div class="modal fade modal-xl" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                          <img src="{{asset($transaction->user->img_path)}}">
                        </div>
                      </div>
      					      <td>{{$transaction->category->name}}</td>
                      <td>{{$transaction->checkInDate}}</td>
                      <td>{{$transaction->checkOutDate}}</td>
                      {{-- action buttons --}}
                      <td>
                        <form method="POST" action="/transactions/{{$transaction->id}}">
                          @csrf
                          @method('PUT')
                          <input type="number" name="approve" value=2 hidden>
                          <button type="submit" class="btn btn-success btn-sm mb-2">Approve</button>
                        </form>
                        <form method="POST" action="/transactions/{{$transaction->id}}">
                          @csrf
                          @method('PUT')
                          <input type="number" name="reject" value=3 hidden>
                          <button type="submit" class="btn btn-danger btn-sm">Reject</button>
                        </form>
                      </td>
      					    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
            @if(session('clear'))
              <div class="alert alert-success" role="alert">
                {{session('clear')}}
              </div>
            @endif
            <h3>Approved Transactions</h3>
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>User</th>
                  <th>Transaction</th>
                  <th>Asset</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Approved On</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($transactions as $transaction)
                  <tr>
                    @if($transaction->status_id === 2)
                      <td>{{$transaction->user->name}}</td>
      					      <td>{{$transaction->refNo}}</td>
                      <td>{{$transaction->asset->serialNo}}</td>
                      <td>{{$transaction->checkInDate}}</td>
                      <td>{{$transaction->checkOutDate}}</td>
                      <td>{{$transaction->updated_at}}</td>
                      {{-- action buttons --}}
                      <td>
                        <form method="POST" action="/transactions/{{$transaction->id}}">
                          @csrf
                          @method('PUT')
                            <input type="number" name="clear" value=4 hidden>
                            <button type="submit" class="btn btn-success btn-sm">Clear</button>
                        </form>
                      </td>
                    @endif
    					    </tr>
                @endforeach
              </tbody>
            </table>
            <h3>Completed Transactions</h3>
            <input class="form-control mb-2" id="myInput" onkeyup="myFunction()" type="text" placeholder="Search user...">
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>User</th>
                  <th>Transaction</th>
                  <th>Asset</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Status</th>
                  <th>Completed On</th>
                </tr>
              </thead>
              <tbody id="myTable">
                @foreach($transactions as $transaction)
                  <tr>
                    @if($transaction->status_id === 3)
                      <td>{{$transaction->user->name}}</td>
      					      <td>{{$transaction->refNo}}</td>
                      <td>-</td>
                      <td>{{$transaction->checkInDate}}</td>
                      <td>{{$transaction->checkOutDate}}</td>
                      <td class="text-danger">{{$transaction->status->name}}</td>
                      <td>{{$transaction->updated_at}}</td>
                    @endif
                    @if($transaction->status_id === 4)
                      <td>{{$transaction->user->name}}</td>
      					      <td>{{$transaction->refNo}}</td>
                      <td>{{$transaction->asset->serialNo}}</td>
                      <td>{{$transaction->checkInDate}}</td>
                      <td>{{$transaction->checkOutDate}}</td>
                      <td class="text-success">{{$transaction->status->name}}</td>
                      <td>{{$transaction->updated_at}}</td>
                    @endif
                    @if($transaction->status_id === 5)
                      <td>{{$transaction->user->name}}</td>
      					      <td>{{$transaction->refNo}}</td>
                      <td>-</td>
                      <td>{{$transaction->checkInDate}}</td>
                      <td>{{$transaction->checkOutDate}}</td>
                      <td class="text-warning">{{$transaction->status->name}}</td>
                      <td>{{$transaction->updated_at}}</td>
                    @endif
    					    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @else
          {{-- user view --}}
          <div class="col-12">
            <div class="jumbotron">
              <h1 class="text-primary">My Transactions</h1>
              <a href="/categories" class="btn btn-info">Dashboard</a>
            </div>
            {{-- check if a session flash variable containing a notification message is set --}}
            @if(session('request'))
              <div class="alert alert-primary" role="alert">
                {{session('request')}}
              </div>
            @endif
            @if(session('cancel'))
              <div class="alert alert-warning" role="alert">
                {{session('cancel')}}
              </div>
            @endif
            <h3>My Pending Requests</h3>
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>Shelter</th>
                  <th>Ref. No.</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Request Date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($transactions as $transaction)
                  @if($transaction->user_id === $user->id)
                    @if($transaction->status_id === 1)
                      <tr>
                        <td>{{$transaction->category->name}}</td>
        					      <td>{{$transaction->refNo}}</td>
                        <td>{{$transaction->checkInDate}}</td>
                        <td>{{$transaction->checkOutDate}}</td>
                        <td>{{$transaction->created_at}}</td>
                        {{-- action buttons --}}
                        <td>
                          <form method="POST" action="/transactions/{{$transaction->id}}">
                            @csrf
                            @method('PUT')
                            @if($transaction->status_id == 1)
                              <input type="number" name="cancel" value=5 hidden>
                              <button type="submit" class="btn btn-warning btn-sm">Cancel</button>
                            @endif
                          </form>
                        </td>
        					    </tr>
                    @endif
                  @endif
                @endforeach
              </tbody>
            </table>
            <h3>My Approved Requests</h3>
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>Ref. No.</th>
                  <th>Asset Serial #</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Approved On</th>
                </tr>
              </thead>
              <tbody>
                @foreach($transactions as $transaction)
                  @if($transaction->user_id === $user->id)
                    <tr>
                      @if($transaction->status_id === 2)
                        <td>{{$transaction->refNo}}</td>
                        <td>{{$transaction->asset->serialNo}}</td>
                        <td>{{$transaction->checkInDate}}</td>
                        <td>{{$transaction->checkOutDate}}</td>
                        <td>{{$transaction->updated_at}}</td>
                      @endif
      					    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
            <h3>My Completed Requests</h3>
            <table class="table table-dark table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>Ref. No.</th>
                  <th>Asset Serial #</th>
                  <th>Ingress Date</th>
                  <th>Egress Date</th>
                  <th>Status</th>
                  <th>Completed On</th>
                </tr>
              </thead>
              <tbody>
                @foreach($transactions as $transaction)
                  @if($transaction->user_id === $user->id)
                    <tr>
                      @if($transaction->status_id === 3)
        					      <td>{{$transaction->refNo}}</td>
                        <td>-</td>
                        <td>{{$transaction->checkInDate}}</td>
                        <td>{{$transaction->checkOutDate}}</td>
                        <td class="text-danger">{{$transaction->status->name}}</td>
                        <td>{{$transaction->updated_at}}</td>
                      @endif
                      @if($transaction->status_id === 4)
        					      <td>{{$transaction->refNo}}</td>
                        <td>{{$transaction->asset->serialNo}}</td>
                        <td>{{$transaction->checkInDate}}</td>
                        <td>{{$transaction->checkOutDate}}</td>
                        <td class="text-success">{{$transaction->status->name}}</td>
                        <td>{{$transaction->updated_at}}</td>
                      @endif
                      @if($transaction->status_id === 5)
        					      <td>{{$transaction->refNo}}</td>
                        <td>-</td>
                        <td>{{$transaction->checkInDate}}</td>
                        <td>{{$transaction->checkOutDate}}</td>
                        <td class="text-warning">{{$transaction->status->name}}</td>
                        <td>{{$transaction->updated_at}}</td>
                      @endif
      					    </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        @endcan
    </div>
  @else
    <div class="jumbotron">
      <h1 class="text-muted">No user is logged in. Please login or register instead.</h1>
    </div>
  @endif
  {{-- javascript --}}
  <script src="{{asset('js/filterUser.js')}}">

  </script>
@endsection
