//get the date today
let today = new Date(new Date().getFullYear(),
                    new Date().getMonth(),
                    new Date().getDate());

//using DOM get the request buttons and the ingress and egress input and notification
const requestBtn = document.querySelector('#requestBtn');
//const inputDates = document.querySelectorAll('.inputDate');
const ingress = document.querySelector('#ingress');
const egress = document.querySelector('#egress');

const ingressNotif = document.querySelector('#ingressNotif');
const egressNotif = document.querySelector('#egressNotif');

//input date - attach an input event listener that
//will enable the add to request button when the input field is not blank.
ingress.addEventListener("input", function(){
  if(ingress.value != "" && ingress.valueAsDate >= today){
    this.parentNode.nextElementSibling.firstElementChild.nextElementSibling.disabled = false; //enable egress input
    ingressNotif.innerHTML = "";
    egress.value = ""; //reset egress input
    egress.addEventListener("input", function(){
      if(ingress.value != "" && egress.valueAsDate >= ingress.valueAsDate && ingress.valueAsDate >= today){
        this.parentNode.nextElementSibling.disabled = false; //enable request button
        egressNotif.innerHTML = "";
      }else{
        this.parentNode.nextElementSibling.disabled = true; //disable request button
        egressNotif.innerHTML = "Warning: Please check your date input.";
      }
    });
  }else{
    egress.value = ""; //reset egress input
    //if the date is not right
    this.parentNode.nextElementSibling.nextElementSibling.disabled = true; //disable egress input
    ingressNotif.innerHTML = "Warning: Please check your date input.";
  }
});
