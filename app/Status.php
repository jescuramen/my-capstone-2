<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //status(single) -> transactions(many)
    public function transactions()
    {
      return $this->hasMany('\App\Transaction');
    }
}
