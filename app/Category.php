<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //category(1) -> assets(many)
    public function assets()
    {
      return $this->hasMany('\App\Asset');
    }

    //category(1) -> transactions(many)
    public function transactions()
    {
      return $this->hasMany('\App\Transaction');
    }

    public function available_assets()
    {
      return $this->hasMany(Asset::class)->where('isAvailable', 1);
    }
}
