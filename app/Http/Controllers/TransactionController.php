<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Category;
use App\Asset;

//import class Auth
use Auth;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $transactions = Transaction::all();
      $user = Auth::user();
      //dd(Auth::user())

      return view('transactions.index')
      ->with('transactions', $transactions)
      ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Transaction::class);


        $request->validate([
          'id' => 'required|integer',
          'ingress' => 'required|date',
          'egress' => 'required|date'
        ]);

        //sanitize our inputs
        $category_id = htmlspecialchars($request->input('id'));
        $ingress = htmlspecialchars($request->input('ingress'));
        $egress = htmlspecialchars($request->input('egress'));
        //dd(date('d/M/Y h:i:s',strtotime($ingress)));

        //instantiate a new transaction
        $transaction = new Transaction;

        $transaction->user_id = Auth::user()->id;
        $transaction->category_id = $category_id;
        $transaction->checkInDate = date('Y-m-d H:i:s', strtotime($ingress));
        $transaction->checkOutDate = date('Y-m-d H:i:s', strtotime($egress));

        //reference number
        $randomString = substr(sha1(time()), 0, 10);
        $refNo = $transaction->category->codename
        . "-" . $randomString . "-" . $transaction->user_id;
        $transaction->refNo = $refNo;

        $transaction->save();

        if($transaction->save()){
          //dd($request->session());
          $request->session()
          ->flash('request', "Your Room request in " . $transaction->category->name . " is being processed.");
        }

        return redirect('/transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //dd(intval($request->input('approve')));

        $approve = intval($request->input('approve'));
        $reject = intval($request->input('reject'));
        $clear = intval($request->input('clear'));
        $cancel = intval($request->input('cancel'));

        //if approve button
        if($approve){
          //make sure the asset is the same as in the transaction
          $endorseAsset = Asset::all()
          ->where('category_id', $transaction->category->id)
          ->where('isAvailable', 1)
          ->first();
          //dd($endorseAsset->id);

          if($endorseAsset->id != null){
            //record to database the new status approved
            $transaction->status_id = $approve;

            //record to database the asset assigned
            $transaction->asset_id = $endorseAsset->id;

            //change the availability of the asset to 0 after endorsement
            $endorseAsset->isAvailable = 0;

            //save the record of the endorsed asset
            $endorseAsset->save();
          }else{
            //if no more assets
            return "No room is available.";
          }
        }

        //if reject button
        if($reject){
          //change the status to reject
          $transaction->status_id = $reject;
        }

        //if clear button
        if($clear){
          //record the new status cleared
          $transaction->status_id = $clear;

          //make the endorsed asset available
          $transaction->asset->isAvailable = 1;
          //save this
          $transaction->asset->save();
        }

        //if cancel button
        if($cancel){
          $transaction->status_id = $cancel;
        }

        $transaction->save();

        if($transaction->save()){
          if($cancel){
            $request->session()->flash('cancel', "You cancelled your room request in " . $transaction->category->name . ".");
          }
          elseif($approve){
            $request->session()->flash('approve', "Room request by " . $transaction->user->name . " in " . $transaction->category->name . " has been approved.");
          }
          elseif($reject){
            $request->session()->flash('reject', "Room request by " . $transaction->user->name . " in " . $transaction->category->name . " has been rejected.");
          }
          elseif($clear){
            $request->session()->flash('clear', "Room used by " . $transaction->user->name . " in " . $transaction->category->name . " has been cleared.");
          }
        }

        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
