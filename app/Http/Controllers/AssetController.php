<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;
//import the class Category
use App\Category;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this
        ->authorize('viewAny', Asset::class)
        ->authorize('viewAny', Category::class);

        $categories = Category::all();
        $assets = Asset::all();


        // $request->validate([
        //   'shelter' => 'required|integer'
        // ]);

        $shelter = htmlspecialchars($request->input('shelter'));



        return view('assets.index')
        ->with('categories', $categories)
        ->with('assets', $assets)
        ->with('shelter', $shelter);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $this->authorize('create', Asset::class);
      $categories = Category::all();
      //send the $categories together with the view for use in assets/create
      return view('assets.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //authorize
        $this->authorize('create', Asset::class);
        //validate form input
        $request->validate([
          //'serialNo' => 'required|string|unique:assets,serialNo',
          'quantity' => 'required|integer',
          'shelter' => 'required|integer'
        ]);

        //sanitize the input
        //$serialNo = htmlspecialchars($request->input('serialNo'));
        $quantity = intval(htmlspecialchars($request->input('quantity')));
        $shelter = htmlspecialchars($request->input('shelter'));
        $duplicate = Category::where('id', $shelter)->first();

        //handle multiple inputs
        for($unit = 1; $unit <= $quantity; $unit++){
          //instantiate a new Asset object
          $asset = new Asset;

          $codename = $duplicate->codename;
          $randomString = substr(sha1(time()), 0, 10);
          $serialNo = $codename . "-" . $randomString . "-" . $unit;

          //assign each value to database asset table
          $asset->serialNo = $serialNo;
          $asset->category_id = $shelter;

          //save the record
          $asset->save();
        }

        //redirect to categories
        return redirect('/assets/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
      $categories = Category::all();
      $assets = Asset::all();

      return view('assets.show')
      ->with('categories', $categories)
      ->with('assets', $assets)
      ->with('shelter', $shelter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Asset $asset)
    {
      if($asset->isMaintained == 1){
        //disable the product in question
        $asset->isMaintained = 0;
      }else{
        //reactivate the product in question
        $asset->isMaintained = 1;
      }

      //save it
      $asset->save();

      return redirect('/assets/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
      if($asset->isAvailable == 1){
        //disable the product in question
        $asset->isAvailable = 0;
      }else{
        //reactivate the product in question
        $asset->isAvailable = 1;
      }

      //save it
      $asset->save();

      return redirect('/assets/');
    }
}
