<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//import Asset model
use App\Asset;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('assets')->get();
        $availableCategories = Category::with('available_assets')->get();
        //dd($availableCategories);
        //dd($categories->assets());//

        //$assets = Asset::all();
        // $assets = Asset::groupBy('isAvailable')
        // ->select('id', 'isAvailable'), \DB::raw('COUNT(*) as cnt');


        // foreach($assets as $asset){
        //   dd($asset->category->isActive);
        // }

        // foreach($categories as $category){
        //   dd($category->assets[0]->isAvailable);
        // }

        return view('categories.index')
        ->with('categories', $categories)
        ->with('availableCategories', $availableCategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //use the authorize() controller helper to check the create() action of the CategoryPolicy class
      //if authorization fails the check, we expect an HTTP response of status code 403
      $this->authorize('create', Category::class);
      return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //handle the form for Fetch
      //implement authorization via Laravel Policy
      $this->authorize('create', Category::class);
      //return $request->all();

      //validate the inputs from our form
      $request->validate([
        'name' => 'required|string|unique:categories,name',
        'codename' => 'required|string|unique:categories,codename',
        'description' => 'required|string',
        'image' => 'required|image'
      ]);

      $name = $request->input('name');
      $codeName = $request->input('codename');
      $description = $request->input('description');
      $image = $request->file('image');


      //validate if category name was received
      if(isset($name)){
        //check if data type is correct and is not empty
        if((gettype($name) === "string") && ($name != "")){
          //selectively query for a category whose name field matches our received $name value.
          //duplicate checking of the input vs the categories table in db and only get the 1st one
            //'name' from categories table
            //$name is the variable above.
          $duplicate = Category::where(strtolower('name'), strtolower($name))->first();
          //if no duplicates found
          if($duplicate === null){
            //sanitize our inputs
            $cleanName = htmlspecialchars($name);
            $cleanCodeName = htmlspecialchars($codeName);
            $cleanDescription = htmlspecialchars($description);

            //handle the file image upload
            $file_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/shelters/";
            $image->move($destination, $file_name);

            //instantiate a new category object from the Category model
            $category = new Category;
            //set the value of the categories table equal to the sanitized form input
            $category->name = $cleanName;
            $category->codename = $codeName;
            $category->description = $description;
            $category->img_path = $destination . $file_name;
            //save the record
            $category->save();
            //redirect to assets/create
            return redirect('/categories/create');
          }
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $isAvailable = 0;
        $notAvailable = 0;
        $assets = $category->assets()->get();
        foreach ($assets as $asset) {
          if ($asset->isAvailable === 1) {
            $isAvailable += 1;
          }else{
            $notAvailable += 1;
          }
        };
        //dd($isAvailable);
        return view('categories.show')
        ->with('category', $category)
        ->with('assets', $assets)
        ->with('isAvailable', $isAvailable)
        ->with('notAvailable', $notAvailable);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
      //die-dump the name of the product to be edited
      //this works due to route-model binding
        //dd($category->name);

      $this->authorize('update', Category::class);
      $assets = Asset::all();

      return view('categories.edit')->with('assets', $assets)->with('category', $category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //authorize
        $this->authorize('update', Category::class);
        //validate the form input
        $request->validate([
          'name' => 'string',
          'codename' => 'string',
          'description' => 'string',
          'image' => 'image'
        ]);
        //sanitize the inputs
        $name = htmlspecialchars($request->input('name'));
        $codeName = htmlspecialchars($request->input('codename'));
        $description = htmlspecialchars($request->input('description'));

        //for image
        $image = $request->file('image');

        //overwrite the properties of $category with input values from the edit form
        $category->name = $name;
        $category->codename = $codeName;
        $category->description = $description;


        //if an image file upload is found, replace the current image of the product with the new upload
        if($request->file('image') != null){
          //handle the file image upload
          //set the filename of the uploaded image to be the time of upload retaining the original file type
          $file_name = time() . "." . $image->getClientOriginalExtension();
          //set target destination where the file will be saved in
          $destination = "images/shelters/";
          //call the move() method of the image object to save the uploaded file in the target destination
          //under the specified file name
          $image->move($destination, $file_name);
          //set the path of the saved image as the value for the column img_path of this record
          $category->img_path = $destination . $file_name;
        }
        $category->save();

        return redirect("/categories/" . $category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
      if($category->isActive == 1){
        //disable the product in question
        $category->isActive = 0;
      }else{
        //reactivate the product in question
        $category->isActive = 1;
      }
      //save it
      $category->save();

      return redirect('/categories/' . $category->id);
    }
}
