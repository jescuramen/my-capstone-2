<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //transactions(many) -> status(single)
    public function status()
    {
      return $this->belongsTo('\App\Status');
    }

    //transactions(many) -> user(single)
    public function user()
    {
      return $this->belongsTo('\App\User');
    }

    //transactions(many) -> asset(single)
    public function asset()
    {
      return $this->belongsTo('\App\Asset');
    }

    //transactions(many) -> category(single)
    public function category()
    {
      return $this->belongsTo('\App\Category');
    }
}
