<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //role(single) -> users(many)
    public function users()
    {
      return $this->hasMany('\App\User');
    }
}
