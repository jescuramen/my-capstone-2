<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     // jes added and img_path here from the users database column
    protected $fillable = [
        'name', 'email', 'password', 'img_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //users(many) -> role(single)
    public function role()
    {
      return $this->belongsTo('\App\Role');
    }

    //user(single) -> transactions(many)
    public function transactions()
    {
      return $this->hasMany('\App\Transaction');
    }
}
