<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'refNo' => NULL,
                'user_id' => 1,
                'status_id' => 4,
                'category_id' => 1,
                'asset_id' => 3,
                'checkInDate' => '2020-04-27 00:00:00',
                'checkOutDate' => '2020-04-30 00:00:00',
                'created_at' => '2020-04-26 16:46:04',
                'updated_at' => '2020-04-27 12:44:27',
            ),
            1 => 
            array (
                'id' => 2,
                'refNo' => 'QCG-e335328b06-3',
                'user_id' => 3,
                'status_id' => 4,
                'category_id' => 2,
                'asset_id' => 12,
                'checkInDate' => '2020-04-28 00:00:00',
                'checkOutDate' => '2020-04-28 00:00:00',
                'created_at' => '2020-04-26 18:50:09',
                'updated_at' => '2020-04-27 14:44:30',
            ),
            2 => 
            array (
                'id' => 3,
                'refNo' => 'QCHB-22534ab326-1',
                'user_id' => 1,
                'status_id' => 5,
                'category_id' => 3,
                'asset_id' => NULL,
                'checkInDate' => '2020-04-29 00:00:00',
                'checkOutDate' => '2020-04-30 00:00:00',
                'created_at' => '2020-04-26 19:00:03',
                'updated_at' => '2020-04-27 12:11:08',
            ),
            3 => 
            array (
                'id' => 4,
                'refNo' => 'QCHB-fba5d585ac-3',
                'user_id' => 3,
                'status_id' => 5,
                'category_id' => 3,
                'asset_id' => NULL,
                'checkInDate' => '2020-04-27 00:00:00',
                'checkOutDate' => '2020-04-27 00:00:00',
                'created_at' => '2020-04-27 12:52:10',
                'updated_at' => '2020-04-27 12:52:21',
            ),
            4 => 
            array (
                'id' => 5,
                'refNo' => 'QCHS-377386600e-3',
                'user_id' => 3,
                'status_id' => 4,
                'category_id' => 1,
                'asset_id' => 4,
                'checkInDate' => '2020-04-27 00:00:00',
                'checkOutDate' => '2020-04-27 00:00:00',
                'created_at' => '2020-04-27 12:53:58',
                'updated_at' => '2020-04-27 12:55:24',
            ),
            5 => 
            array (
                'id' => 6,
                'refNo' => 'QCP-9c9adea9b1-1',
                'user_id' => 1,
                'status_id' => 4,
                'category_id' => 4,
                'asset_id' => 31,
                'checkInDate' => '2020-04-27 00:00:00',
                'checkOutDate' => '2020-04-27 00:00:00',
                'created_at' => '2020-04-27 13:05:42',
                'updated_at' => '2020-04-27 13:07:30',
            ),
            6 => 
            array (
                'id' => 7,
                'refNo' => 'QCHB-a832a3073e-1',
                'user_id' => 1,
                'status_id' => 3,
                'category_id' => 3,
                'asset_id' => NULL,
                'checkInDate' => '2020-04-27 00:00:00',
                'checkOutDate' => '2020-04-27 00:00:00',
                'created_at' => '2020-04-27 13:27:56',
                'updated_at' => '2020-04-27 14:43:42',
            ),
            7 => 
            array (
                'id' => 8,
                'refNo' => 'QCP-5d6949ecf5-3',
                'user_id' => 3,
                'status_id' => 4,
                'category_id' => 4,
                'asset_id' => 31,
                'checkInDate' => '2020-04-28 00:00:00',
                'checkOutDate' => '2020-04-29 00:00:00',
                'created_at' => '2020-04-27 17:17:07',
                'updated_at' => '2020-04-28 09:23:02',
            ),
            8 => 
            array (
                'id' => 9,
                'refNo' => 'QCHB-093a130b65-3',
                'user_id' => 3,
                'status_id' => 2,
                'category_id' => 3,
                'asset_id' => 18,
                'checkInDate' => '2020-04-28 00:00:00',
                'checkOutDate' => '2020-04-29 00:00:00',
                'created_at' => '2020-04-27 18:35:23',
                'updated_at' => '2020-04-28 09:21:32',
            ),
            9 => 
            array (
                'id' => 10,
                'refNo' => 'QCP-e993c6cc8b-1',
                'user_id' => 1,
                'status_id' => 2,
                'category_id' => 4,
                'asset_id' => 31,
                'checkInDate' => '2020-04-28 00:00:00',
                'checkOutDate' => '2020-04-29 00:00:00',
                'created_at' => '2020-04-27 18:56:24',
                'updated_at' => '2020-04-28 09:59:13',
            ),
            10 => 
            array (
                'id' => 11,
                'refNo' => 'QCG-95a64ed871-1',
                'user_id' => 1,
                'status_id' => 3,
                'category_id' => 2,
                'asset_id' => NULL,
                'checkInDate' => '2020-04-29 00:00:00',
                'checkOutDate' => '2020-04-30 00:00:00',
                'created_at' => '2020-04-28 09:20:28',
                'updated_at' => '2020-04-28 09:21:43',
            ),
            11 => 
            array (
                'id' => 12,
                'refNo' => 'QCP-8e7c83bd03-1',
                'user_id' => 1,
                'status_id' => 1,
                'category_id' => 4,
                'asset_id' => NULL,
                'checkInDate' => '2020-04-28 00:00:00',
                'checkOutDate' => '2020-04-30 00:00:00',
                'created_at' => '2020-04-28 14:54:40',
                'updated_at' => '2020-04-28 14:54:40',
            ),
        ));
        
        
    }
}