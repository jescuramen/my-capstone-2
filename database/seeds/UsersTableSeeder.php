<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'jes',
                'email' => 'jes@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$4xBCoGWKjO9ipzq8cTutVe4uDivz2qJdWgp5AOwc9Ji8UFN0tF62a',
                'remember_token' => NULL,
                'img_path' => 'images/1587401828.jpg',
                'role_id' => 1,
                'created_at' => '2020-04-20 16:57:08',
                'updated_at' => '2020-04-20 16:57:08',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$FTQhEnCF5LqoVWKIsKmZAOiTCMkxk/QYRniZU5pR.axfqDaTh0.TO',
                'remember_token' => NULL,
                'img_path' => NULL,
                'role_id' => 2,
                'created_at' => '2020-04-20 17:01:07',
                'updated_at' => '2020-04-20 17:01:07',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'ivy',
                'email' => 'ivy@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$cW92TQ9Wh967BcIll2oSXO2zOQaFnM8jQQey32riLxgtS4uEejDq6',
                'remember_token' => NULL,
                'img_path' => 'images/1587402600.jpg',
                'role_id' => 1,
                'created_at' => '2020-04-20 17:10:00',
                'updated_at' => '2020-04-20 17:10:00',
            ),
        ));


    }
}
