<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'serialNo' => 'QCHS-89b97931b8-1',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-26 11:55:14',
            ),
            1 => 
            array (
                'id' => 2,
                'serialNo' => 'QCHS-89b97931b8-2',
                'isAvailable' => 0,
                'isMaintained' => 0,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-25 11:58:08',
            ),
            2 => 
            array (
                'id' => 3,
                'serialNo' => 'QCHS-89b97931b8-3',
                'isAvailable' => 1,
                'isMaintained' => 0,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-25 12:04:24',
            ),
            3 => 
            array (
                'id' => 4,
                'serialNo' => 'QCHS-89b97931b8-4',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            4 => 
            array (
                'id' => 5,
                'serialNo' => 'QCHS-89b97931b8-5',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            5 => 
            array (
                'id' => 6,
                'serialNo' => 'QCHS-89b97931b8-6',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            6 => 
            array (
                'id' => 7,
                'serialNo' => 'QCHS-89b97931b8-7',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            7 => 
            array (
                'id' => 8,
                'serialNo' => 'QCHS-89b97931b8-8',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            8 => 
            array (
                'id' => 9,
                'serialNo' => 'QCHS-89b97931b8-9',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            9 => 
            array (
                'id' => 10,
                'serialNo' => 'QCHS-89b97931b8-10',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 08:48:01',
                'updated_at' => '2020-04-24 08:48:01',
            ),
            10 => 
            array (
                'id' => 11,
                'serialNo' => 'QCG-7e58a493e1-1',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 08:48:08',
                'updated_at' => '2020-04-25 12:25:51',
            ),
            11 => 
            array (
                'id' => 12,
                'serialNo' => 'QCG-7e58a493e1-2',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 08:48:08',
                'updated_at' => '2020-04-24 08:48:08',
            ),
            12 => 
            array (
                'id' => 13,
                'serialNo' => 'QCG-7e58a493e1-3',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 08:48:08',
                'updated_at' => '2020-04-25 12:05:59',
            ),
            13 => 
            array (
                'id' => 14,
                'serialNo' => 'QCG-7e58a493e1-4',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 08:48:08',
                'updated_at' => '2020-04-24 08:48:08',
            ),
            14 => 
            array (
                'id' => 15,
                'serialNo' => 'QCG-7e58a493e1-5',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 08:48:08',
                'updated_at' => '2020-04-24 08:48:08',
            ),
            15 => 
            array (
                'id' => 16,
                'serialNo' => 'QCHB-e0e3ad531f-1',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-25 12:25:46',
            ),
            16 => 
            array (
                'id' => 17,
                'serialNo' => 'QCHB-e0e3ad531f-2',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-25 12:26:39',
            ),
            17 => 
            array (
                'id' => 18,
                'serialNo' => 'QCHB-e0e3ad531f-3',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            18 => 
            array (
                'id' => 19,
                'serialNo' => 'QCHB-e0e3ad531f-4',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            19 => 
            array (
                'id' => 20,
                'serialNo' => 'QCHB-e0e3ad531f-5',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            20 => 
            array (
                'id' => 21,
                'serialNo' => 'QCHB-e0e3ad531f-6',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            21 => 
            array (
                'id' => 22,
                'serialNo' => 'QCHB-e0e3ad531f-7',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            22 => 
            array (
                'id' => 23,
                'serialNo' => 'QCHB-e0e3ad531f-8',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            23 => 
            array (
                'id' => 24,
                'serialNo' => 'QCHB-e0e3ad531f-9',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            24 => 
            array (
                'id' => 25,
                'serialNo' => 'QCHB-e0e3ad531f-10',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            25 => 
            array (
                'id' => 26,
                'serialNo' => 'QCHB-e0e3ad531f-11',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            26 => 
            array (
                'id' => 27,
                'serialNo' => 'QCHB-e0e3ad531f-12',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            27 => 
            array (
                'id' => 28,
                'serialNo' => 'QCHB-e0e3ad531f-13',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            28 => 
            array (
                'id' => 29,
                'serialNo' => 'QCHB-e0e3ad531f-14',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            29 => 
            array (
                'id' => 30,
                'serialNo' => 'QCHB-e0e3ad531f-15',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 08:48:17',
                'updated_at' => '2020-04-24 08:48:17',
            ),
            30 => 
            array (
                'id' => 31,
                'serialNo' => 'QCP-4a61d3d061-1',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            31 => 
            array (
                'id' => 32,
                'serialNo' => 'QCP-4a61d3d061-2',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            32 => 
            array (
                'id' => 33,
                'serialNo' => 'QCP-4a61d3d061-3',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            33 => 
            array (
                'id' => 34,
                'serialNo' => 'QCP-4a61d3d061-4',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            34 => 
            array (
                'id' => 35,
                'serialNo' => 'QCP-4a61d3d061-5',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            35 => 
            array (
                'id' => 36,
                'serialNo' => 'QCP-4a61d3d061-6',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            36 => 
            array (
                'id' => 37,
                'serialNo' => 'QCP-4a61d3d061-7',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            37 => 
            array (
                'id' => 38,
                'serialNo' => 'QCP-4a61d3d061-8',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            38 => 
            array (
                'id' => 39,
                'serialNo' => 'QCP-4a61d3d061-9',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            39 => 
            array (
                'id' => 40,
                'serialNo' => 'QCP-4a61d3d061-10',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            40 => 
            array (
                'id' => 41,
                'serialNo' => 'QCP-4a61d3d061-11',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            41 => 
            array (
                'id' => 42,
                'serialNo' => 'QCP-4a61d3d061-12',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            42 => 
            array (
                'id' => 43,
                'serialNo' => 'QCP-4a61d3d061-13',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            43 => 
            array (
                'id' => 44,
                'serialNo' => 'QCP-4a61d3d061-14',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            44 => 
            array (
                'id' => 45,
                'serialNo' => 'QCP-4a61d3d061-15',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            45 => 
            array (
                'id' => 46,
                'serialNo' => 'QCP-4a61d3d061-16',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            46 => 
            array (
                'id' => 47,
                'serialNo' => 'QCP-4a61d3d061-17',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            47 => 
            array (
                'id' => 48,
                'serialNo' => 'QCP-4a61d3d061-18',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            48 => 
            array (
                'id' => 49,
                'serialNo' => 'QCP-4a61d3d061-19',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            49 => 
            array (
                'id' => 50,
                'serialNo' => 'QCP-4a61d3d061-20',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            50 => 
            array (
                'id' => 51,
                'serialNo' => 'QCP-4a61d3d061-21',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            51 => 
            array (
                'id' => 52,
                'serialNo' => 'QCP-4a61d3d061-22',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            52 => 
            array (
                'id' => 53,
                'serialNo' => 'QCP-4a61d3d061-23',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            53 => 
            array (
                'id' => 54,
                'serialNo' => 'QCP-4a61d3d061-24',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            54 => 
            array (
                'id' => 55,
                'serialNo' => 'QCP-4a61d3d061-25',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            55 => 
            array (
                'id' => 56,
                'serialNo' => 'QCP-4a61d3d061-26',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            56 => 
            array (
                'id' => 57,
                'serialNo' => 'QCP-4a61d3d061-27',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            57 => 
            array (
                'id' => 58,
                'serialNo' => 'QCP-4a61d3d061-28',
                'isAvailable' => 1,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-24 08:48:23',
            ),
            58 => 
            array (
                'id' => 59,
                'serialNo' => 'QCP-4a61d3d061-29',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-25 12:26:51',
            ),
            59 => 
            array (
                'id' => 60,
                'serialNo' => 'QCP-4a61d3d061-30',
                'isAvailable' => 0,
                'isMaintained' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 08:48:23',
                'updated_at' => '2020-04-25 12:16:36',
            ),
        ));
        
        
    }
}