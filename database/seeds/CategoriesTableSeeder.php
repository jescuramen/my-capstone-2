<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'codename' => 'QCHS',
                'created_at' => '2020-04-22 23:33:29',
                'description' => 'School building converted to a temporary shelter for front-liners located in quezon city.',
                'id' => 1,
                'img_path' => 'images/shelters/1587598409.jpg',
                'isActive' => 1,
                'name' => 'Quezon City High School',
                'updated_at' => '2020-04-24 06:37:18',
            ),
            1 => 
            array (
                'codename' => 'QCG',
                'created_at' => '2020-04-22 23:34:08',
                'description' => 'Gymnasium building converted to a temporary shelter for front-liners located in quezon city.',
                'id' => 2,
                'img_path' => 'images/shelters/1587598448.jpeg',
                'isActive' => 1,
                'name' => 'Quezon City Gymnasium',
                'updated_at' => '2020-04-24 06:27:01',
            ),
            2 => 
            array (
                'codename' => 'QCHB',
                'created_at' => '2020-04-22 23:34:39',
                'description' => 'Government building converted to a temporary shelter for front-liners located in quezon city.',
                'id' => 3,
                'img_path' => 'images/shelters/1587598479.jpeg',
                'isActive' => 1,
                'name' => 'Quezon City Hall Building',
                'updated_at' => '2020-04-24 06:27:09',
            ),
            3 => 
            array (
                'codename' => 'QCP',
                'created_at' => '2020-04-22 23:35:26',
                'description' => 'A park converted into a temporary shelter for front-liners located in quezon city.',
                'id' => 4,
                'img_path' => 'images/shelters/1587598526.jpeg',
                'isActive' => 1,
                'name' => 'Quezon City Park',
                'updated_at' => '2020-04-23 12:17:34',
            ),
        ));
        
        
    }
}