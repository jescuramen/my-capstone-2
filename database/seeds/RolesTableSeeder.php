<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'frontliner',
                'created_at' => '2020-04-21 00:55:58',
                'updated_at' => '2020-04-21 00:55:58',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'admin',
                'created_at' => '2020-04-21 00:55:58',
                'updated_at' => '2020-04-21 00:55:58',
            ),
        ));
        
        
    }
}